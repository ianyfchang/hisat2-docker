FROM biocontainers/biocontainers:latest


RUN conda install -c bioconda samtools hisat2 tophat=2.1.0 bowtie bowtie2 vcftools bcftools
